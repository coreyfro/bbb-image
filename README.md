Series One 2014 Image Builder
=============================

This Multistrap system builds linux root filesystem images (currently) based on Ubuntu for use with the BeagleBone Black and the Series One 2014 3D printer or other 3D printer models. User must supply a u-boot image seperately, and together with the roofts the images can be written to a drive with the flash.sh or sdflash.sh bash scripts  

At the time of writing we use Ubuntu Trusty Tahr (14.04, tbe) on various architectures for builds, and so far it looks like it's working well.

Building Images with Multistrap
===============================

This version requires moderate understanding of the command line.  

Use apt-get (or aptitude or the package manager for your system as you like) to add the following packages
multistrap (currently 2.2.0ubuntu1)
binfmt-support
qemu
qemu-user-static 

The version of Multistrap perlscript listed above needs to be modified to run.
The current fix is to add $forceyes to the declared variables on line 30 of /usr/sbin/multistrap, but
there's a better way even though the fix hasn't been included in Trusty yet.  :-(
https://code.launchpad.net/~danlipsitt/ubuntu/trusty/multistrap/bug-1313787

If you have this running on other platforms (arch, osx, cygwin, or natively on the bbb, etc), 
then we would love to hear from you on our forums at forum.typeamachines.com 
or at support@typeamachines.com  


Process to build an image with multistrap
========================================

Note that the steps can require environment variables to be declared before execution

1. Build an image with the script called `bbb-image.sh`. At the command line prompt of your build system, type the following command stanza:

sudo dist=trusty bash bbb-image.sh

2. Prepare an SD card:  Edit sdflash (expand this) and flash the image to an SD card with `sdflash.sh` (it simply calls up `flash.sh`). This script currently assumes all sd cards are enumerated as a device at /dev/mmcblk0 and partitions at mmcblk0pN where N is the partition number, again at the command line prompt of your system, type in the following:

sudo bash sdflash.sh

3. Configure the image with `numerate.sh`. Numerate gives the machine a name, or a number, if you're into that sort of thing.

numerate.sh